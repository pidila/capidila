$(document).ready (function() {
    var tabbables = "a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex]",
    	lastFocus;

    // 1. Transformation des liens en boutons
    $('a.link-to-modal').each(function(){
        var triggerId 			= $(this).attr('id'),
	    	triggerClass 	    = $(this).attr('class') + ' modal-trigger';

        $button = $('<button>', {
            'class': triggerClass,
            'id': triggerId
        });
    	// -- Output du bouton
    	$(this).contents().unwrap().wrap($button);
	});

    // 1bis. On enlève les class inutiles des boutons ou liens
    $('.modal-trigger').removeClass('link-to-modal');
    if ( $('.modal-trigger').hasClass('hidden-if-nojs') ) {
        $('.modal-trigger').removeClass('hidden-if-nojs');
    }

	// 2. Attributs aria sur les modales - leurs titres en h1
    $('.modal').each(function() {
		var modalTitle 			= $(this).attr('id') + '-title',
            modalTitleId		= '#' + modalTitle;

    	// ajout des attributs à la modale, fermées par défaut
        $(this).attr({'role':'dialog', 'aria-labelledby':modalTitle, 'aria-hidden':'true'}).addClass('modal-is-closed');
        // transformation du titre de la modale en h1
        $(modalTitleId).replaceWith('<h1 class="modal-title" id="'+ modalTitle +'">' + $(modalTitleId).html() + '</h1>');
    });

	// 3. Création des boutons de fermeture
	$('.modal').each(function() {
		$('<button class="modal-closer" aria-label="fermer cette fenêtre"><span role="presentation">&times;</span></button>').appendTo($(this));
	});

	// 4. Actions à l'ouverture
	$('.modal-trigger').click(function() {
		// pour récupérer l'id de la modale correspondante
		var openedModal = '#' + $(this).attr('id').replace('-trigger','');
        // memorisation focus
		lastFocus = document.activeElement;

  		// Enveloppement et nouveaux attributs sur la modale ouverte
        $(openedModal).appendTo('body').wrap('<div class="modal-overlay"></div>').removeClass('modal-is-closed').addClass('modal-is-open').removeAttr('aria-hidden');
  		// nouveaux attributs sur #page si une modale est ouverte
  		$('#page').attr('aria-hidden','true').addClass('has-open-modal');

		// Restriction du focus à la modale ouverte
		$(tabbables).each(function() {
            var element = $(this),
                tabIndex = element.attr('tabIndex');
            // stockage du tabindex pour restitution après fermeture
            if(tabIndex !== undefined) {
                element.attr('data-tabindex', tabIndex);
            }
            element.attr('tabIndex', -1);
        });
		$(openedModal).find(tabbables).attr('tabindex', 0).first().focus();  		
   	});

	// 5. Actions à la fermeture
	$('.modal-closer').click(function() {
		// pour identification de la modale parent
		var myModal = $(this).parent('.modal'),
			myModalId = '#' + $(myModal).attr('id');

		// restauration des attributs respectifs de la modale et de #page
  		$(myModalId).removeClass('modal-is-open').addClass('modal-is-closed').attr('aria-hidden','true');
  		$('#page').removeAttr('aria-hidden').removeClass('has-open-modal');
        // unwrap seulement si le parent est overlay (sinon escape plante)
        $('.modal-overlay > .modal').unwrap();

		// Restitution du des tabindex originels aux éléments du body…
		$(tabbables).each(function(){
            var element = $(this),
                tabIndex = element.data('tabindex');

            if (tabIndex !== undefined) {
                element.attr('tabIndex', tabIndex);
            } else {
                element.removeAttr('tabIndex');
            }
            element.removeAttr('data-tabindex');
        });
        // … et replacement du focus sur le trigger
  		lastFocus.focus();
	});

	// 5bis. Sortir de la modale avec Cancel
	$('.cancel').click(function(e) {
        e.preventDefault();
		$('.modal-closer').click();
        // todo: recuperer le focus sur le trigger
	});

	// 5ter. Sortir de la modale avec Escape
	$(window).keydown(function(ev) {
		if (ev.which == 27 && $('.modal-is-open'))  {
			$('.modal-closer').click();
		}
	});
});
// todo: tester sur différents navigateurs et aides techniques