# Composants html/js + ARIA conformes au RGAA3

![capidila.png](https://bitbucket.org/repo/7Xprz8/images/1899813373-capidila.png)

Ces composants ont pour vocation de pouvoir être réutilisés, les démos sont fonctionnelles séparément, les deux seuls fichiers partagés sont style.css et jquery-min.js (jQuery v1.12.1).

Les fichiers de chaque composant sont réunis dans un sous-répertoire de ce dépôt. Nomenclature de nommage des fichiers index.html, <composant>.js, <composant>.css (s'il y a lieu).

## Liste des composants disponibles
- collapse: plier et déplier des contenus (sans système d'accordéon)
- modale: extrait du contenu d'une div de la page en overlay

## Contributions

Vous pouvez ouvrir un ticket pour proposer des améliorations ou signaler des bugs ; mieux encore, vous pouvez accompagner le ticket avec un pull request !